package com.example.abhishektiwari.splashscreen;

/**
 * Created by Abhishek Tiwari on 12/30/2015.
 */
public class EventListItem {
    boolean expanded = false;

    void changeView() {
        expanded = !expanded;
    }

    boolean isExpanded() {
        return expanded;
    }
}
