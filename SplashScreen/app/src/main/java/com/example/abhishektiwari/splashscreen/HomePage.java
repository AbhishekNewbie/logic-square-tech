package com.example.abhishektiwari.splashscreen;


import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HomePage extends ActionBarActivity {
    final static int NUMBER_OF_CIRCLES = 7;
    int BiggerCircleRadius;
    int SmallerCircleRadius;
    Point BiggerCircleCenter;
    RelativeLayout circles[];
    RelativeLayout BigStaticCircle;
    RelativeLayout selected_menu_item_pointer;
    int[] circle_ids;
    int highlighted_circle_index;
    Point size;
    mPoint newPoint;
    private mPoint[] points;

    TextView currentItem;
    RelativeLayout adderButton;

    //there maybe other ways to set this.
    //for sake of completeness and uniformity, setting pos_values as set for others
    RelativeLayout side_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wheely_menu);
        currentItem = (TextView) findViewById(R.id.current_item);
        adderButton = (RelativeLayout) findViewById(R.id.adder);
        side_menu = (RelativeLayout) findViewById(R.id.side_menu);
        //TODO: for different versions, this may need different approach
        //TODO: write code for each android version.
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        setCircles();
        newPoint = new mPoint();
        onTouchMovement();
        ViewGroup.LayoutParams params;
        adderButton.setX(selected_menu_item_pointer.getX());
        adderButton.setY(size.y * 4 / 5);
        params = adderButton.getLayoutParams();
        params.height = 2 * SmallerCircleRadius;
        params.width = params.height;
        adderButton.setLayoutParams(params);
        side_menu.setX(10);
        side_menu.setY(10);
        params = side_menu.getLayoutParams();
        params.height = SmallerCircleRadius;
        params.width = params.height;
        side_menu.setLayoutParams(params);
        currentItem.setX(BiggerCircleCenter.x);
        currentItem.setY(BiggerCircleCenter.y + BiggerCircleRadius + SmallerCircleRadius);
        BigStaticCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(HomePage.this,EventsList.class);
                startActivity(intent);
            }
        });
    }


    private void onTouchMovement() {
        for (int i = 0; i < NUMBER_OF_CIRCLES; i++) {
            final int finalI = i;
            circles[i].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    if (view.getId() != circle_ids[finalI]) return false;
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            newPoint.x = event.getRawX() - BiggerCircleCenter.x;
                            newPoint.y = -1.0f * event.getRawY() + BiggerCircleCenter.y;
                            _("Y:" + newPoint.y);
                            float mag = (float) Math.sqrt(newPoint.x * newPoint.x + newPoint.y * newPoint.y);
                            mPoint pointOnCircle = new mPoint();
                            pointOnCircle.x = newPoint.x * BiggerCircleRadius / mag;
                            pointOnCircle.y = newPoint.y * BiggerCircleRadius / mag;
                            adjust(pointOnCircle, finalI);
                            _("on circle:" + pointOnCircle.y);
                    }
                    setCurrentItem();
                    return true;
                }
            });

        }
    }

    private void setCurrentItem() {
        float min_diff = 10000;
        int index = 0;
        float temp;
        for (int i = 0; i < NUMBER_OF_CIRCLES; i++) {
            temp = Math.abs(points[i].y - BiggerCircleCenter.y);
            if (temp < min_diff && points[i].x >= 0) {
                min_diff = temp;
                index = i;
            }
        }
        HighlightCircle(index);
        RemoveHighlightCircle(highlighted_circle_index);

    }


    private void adjust(mPoint currentPoint, int changedCircle) {
        points[changedCircle].x = currentPoint.x + BiggerCircleCenter.x;
        points[changedCircle].y = -currentPoint.y + BiggerCircleCenter.y;
        int count = 0;
        int i = changedCircle;
        float theta, phi;
        theta = (float) (2 * Math.PI / NUMBER_OF_CIRCLES);
        mPoint temp = new mPoint();
        temp.x = currentPoint.x;
        temp.y = currentPoint.y;
        while (count != NUMBER_OF_CIRCLES - 1) {
            phi = temp.getAngle();
            i = (i + 1) % NUMBER_OF_CIRCLES;
            // _("Point:(" + temp.x + "," + temp.y + ") angle:" + (phi * 180 / Math.PI));
            temp.x = (float) (BiggerCircleRadius * Math.cos(theta + phi));
            temp.y = (float) (BiggerCircleRadius * Math.sin(theta + phi));
            points[i].x = temp.x + BiggerCircleCenter.x;
            points[i].y = -temp.y + BiggerCircleCenter.y;
            count++;
        }

        for (i = 0; i < NUMBER_OF_CIRCLES; i++) {
            circles[i].setX(points[i].x - SmallerCircleRadius);
            circles[i].setY(points[i].y - SmallerCircleRadius);
        }

    }

    private void setCircles() {
        circles = new RelativeLayout[NUMBER_OF_CIRCLES];
        circle_ids = new int[NUMBER_OF_CIRCLES];
        circle_ids[0] = R.id.circle0;
        circle_ids[1] = R.id.circle1;
        circle_ids[2] = R.id.circle2;
        circle_ids[3] = R.id.circle3;
        circle_ids[4] = R.id.circle4;
        circle_ids[5] = R.id.circle5;
        circle_ids[6] = R.id.circle6;
        circles[0] = (RelativeLayout) findViewById(R.id.circle0);
        circles[1] = (RelativeLayout) findViewById(R.id.circle1);
        circles[2] = (RelativeLayout) findViewById(R.id.circle2);
        circles[3] = (RelativeLayout) findViewById(R.id.circle3);
        circles[4] = (RelativeLayout) findViewById(R.id.circle4);
        circles[5] = (RelativeLayout) findViewById(R.id.circle5);
        circles[6] = (RelativeLayout) findViewById(R.id.circle6);
        BigStaticCircle = (RelativeLayout) findViewById(R.id.static_circle);
        Display display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        BiggerCircleCenter = new Point();
        BiggerCircleCenter.x = size.x / 10;
        BiggerCircleCenter.y = size.y / 2;
        BiggerCircleRadius = (int) ((0.45f) * size.x);
        SmallerCircleRadius = (int) ((0.09f) * size.x);
        BigStaticCircle.setX(BiggerCircleCenter.x - BiggerCircleRadius);
        BigStaticCircle.setY(BiggerCircleCenter.y - BiggerCircleRadius);
        ViewGroup.LayoutParams params = BigStaticCircle.getLayoutParams();
        params.width = 2 * BiggerCircleRadius;
        params.height = params.width;
        BigStaticCircle.setLayoutParams(params);
        mPoint[] co_ords = getCircleCoordinates(BiggerCircleCenter, NUMBER_OF_CIRCLES, BiggerCircleRadius, 0);
        for (int i = 0; i < NUMBER_OF_CIRCLES; i++) {
            circles[i].setX((float) co_ords[i].x - SmallerCircleRadius);
            circles[i].setY((float) co_ords[i].y - SmallerCircleRadius);
            params = circles[i].getLayoutParams();
            params.width = 2 * SmallerCircleRadius;
            params.height = params.width;
            circles[i].setLayoutParams(params);
            circles[i].invalidate();

        }
        selected_menu_item_pointer = (RelativeLayout) findViewById(R.id.current_item_pointer);
        params = selected_menu_item_pointer.getLayoutParams();
        params.height = SmallerCircleRadius;
        params.width = SmallerCircleRadius;
        selected_menu_item_pointer.setX(circles[0].getX() + size.x / 7 + SmallerCircleRadius);
        selected_menu_item_pointer.setY(BiggerCircleCenter.y - params.height / 2);
        selected_menu_item_pointer.setLayoutParams(params);
        highlighted_circle_index = 0;
        HighlightCircle(highlighted_circle_index);
    }

    //TODO: set highlighted image
    private void HighlightCircle(int highlighted_circle_index) {
        //  circles[highlighted_circle_index].setBackground(Drawable.createFromPath("@Drawable/menu_image_half"));
        String[] MenuItemNames = new String[NUMBER_OF_CIRCLES];
        MenuItemNames[0] = "ITEM0";
        MenuItemNames[1] = "ITEM1";
        MenuItemNames[2] = "ITEM2";
        MenuItemNames[3] = "ITEM3";
        MenuItemNames[4] = "ITEM4";
        MenuItemNames[5] = "ITEM5";
        MenuItemNames[6] = "ITEM6";
        currentItem.setText(MenuItemNames[highlighted_circle_index]);
    }

    // TODO: set the previous "unhighlighted" image here
    private void RemoveHighlightCircle(int index) {


    }

    private mPoint[] getCircleCoordinates(Point center, int num, int R, float initial_theta) {
        points = new mPoint[num];
        //initial_theta+=Math.PI/2;
        float theta = (float) (2.0 * Math.PI / num);
        mPoint temp;
        for (int i = 0; i < num; i++) {
            temp = new mPoint();
            temp.x = (float) (center.x + R * Math.cos(theta * i - initial_theta));
            temp.y = (float) (center.y + R * Math.sin(theta * i - initial_theta));
            points[i] = temp;
        }
        return points;
    }

    void _(String str) {
        System.out.println(" " + str);
    }

    private class mPoint {
        float x;
        float y;

        float getAngle() {
            //_("x and y "+x+","+y);
            float phi;
            float x = this.x > 0 ? this.x : this.x * -1;
            float y = this.y > 0 ? this.y : this.y * -1;
            phi = (float) Math.atan((double) Math.abs(y) / (double) Math.abs(x));
            if (this.y >= 0 && this.x >= 0)
                return phi;
            else if (this.y >= 0 && this.x < 0)
                return (float) (Math.PI - phi);
            else if (this.y < 0 && this.x < 0)
                return (float) (Math.PI + phi);
            else
                return (float) (2 * Math.PI - phi);


        }
    }

}
