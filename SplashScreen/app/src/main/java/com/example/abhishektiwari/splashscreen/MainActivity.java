package com.example.abhishektiwari.splashscreen;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends ActionBarActivity {

    LinearLayout splashScreen, phoneVerificationLayout, VerificationSuccessfulLayout, FGRegistrationLayout;
    ImageView verifyNumberButton, FinalRegistrationPage, FacebookRegistration, GoogleRegistration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO: fix this for each version of android.
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.hide();
        } catch (NullPointerException e) {
            System.out.println("Version conflict.");
        }

        setContentView(R.layout.activity_main);
        splashScreen = (LinearLayout) findViewById(R.id.splash_id);
        phoneVerificationLayout = (LinearLayout) findViewById(R.id.phone_verification_layout);
        verifyNumberButton = (ImageView) findViewById(R.id.verify_number_button);
        VerificationSuccessfulLayout = (LinearLayout) findViewById(R.id.verification_successful_layout);
        FinalRegistrationPage = (ImageView) findViewById(R.id.final_registration_page);
        FGRegistrationLayout = (LinearLayout) findViewById(R.id.facebook_google_registration_layout);
        FacebookRegistration = (ImageView) findViewById(R.id.facebook_registration_page);
        GoogleRegistration = (ImageView) findViewById(R.id.google_registration_page);

        splashScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                splashScreen.setVisibility(View.GONE);
                phoneVerificationLayout.setVisibility(View.VISIBLE);
            }
        });

        verifyNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: verify phone number and show next screen
                System.out.println("Getting clicked on this view");
                VerificationSuccessfulLayout.setVisibility(View.VISIBLE);
                phoneVerificationLayout.setVisibility(View.GONE);
            }
        });

        FinalRegistrationPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VerificationSuccessfulLayout.setVisibility(View.GONE);
                FGRegistrationLayout.setVisibility(View.VISIBLE);
            }
        });

        FacebookRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO : FACEBOOK REGISTRATION
                OpenHomePage();
            }
        });
        GoogleRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO : GOOGLE REGISTRATION
                OpenHomePage();
            }
        });
    }

    private void OpenHomePage() {
        Intent intent = new Intent(MainActivity.this, HomePage.class);
        startActivity(intent);
        finish();
    }


}
