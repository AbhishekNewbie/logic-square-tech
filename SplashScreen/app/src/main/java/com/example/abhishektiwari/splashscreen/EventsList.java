package com.example.abhishektiwari.splashscreen;

import java.util.ArrayList;
import java.util.Objects;

import android.app.ExpandableListActivity;
import android.app.ListActivity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;

public class EventsList extends ListActivity {
    private ArrayList<EventListItem> listItems = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_page);
        ListView expandableList = (ListView) findViewById(android.R.id.list);
        setListItems();
        CustomAdapter adapter = new CustomAdapter(this, listItems, getResources());
        expandableList.setAdapter(adapter);
    }

    public void setListItems() {
        //TODO: add items which are visible in 'unexpanded list view'
        listItems.add(new EventListItem());
        listItems.add(new EventListItem());
        listItems.add(new EventListItem());
    }

    void _(Object str) {
        Log.d("Error", "" + str);
    }
}